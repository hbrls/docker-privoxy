Docker Image
==

    $ docker build -t hbrls/privoxy .

Howto
==

    $ docker run -d -p 8118:3108 -v /path/to/shadowsocks.json:/shadowsocks.json -v /path/to/log:/var/log/supervisor --name privoxy hbrls/privoxy
    $ curl --proxy 127.0.0.1:8118 https://www.google.com

